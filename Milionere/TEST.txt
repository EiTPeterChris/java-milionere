{
    "message": "OK",
    "status": 200,
    "data": {
        "packageType": {
            "id": "packageType",
            "name": "Typ usługi pakietowej",
            "type": "enum",
            "values": [
                {
                    "id": "city_break",
                    "name": "City Break"
                },
                {
                    "id": "2in1",
                    "name": "Dwa w jednym"
                },
                {
                    "id": "narty",
                    "name": "Narty"
                },
                {
                    "id": "no_limits",
                    "name": "No Limits"
                },
                {
                    "id": "wczasy",
                    "name": "Wczasy"
                },
                {
                    "id": "wycieczka",
                    "name": "Wycieczka"
                }
            ]
        },
        "departureRegion": {
            "id": "departureRegion",
            "name": "Skšd",
            "type": "enum",
            "values": [
                {
                    "id": "bydgoszcz",
                    "name": "Bydgoszcz",
                    "departureCodes": [
                        "BYD",
                        "BZG"
                    ]
                },
                {
                    "id": "gdansk",
                    "name": "Gdańsk",
                    "departureCodes": [
                        "GDA",
                        "GDN"
                    ]
                },
                {
                    "id": "katowice",
                    "name": "Katowice",
                    "departureCodes": [
                        "KCW",
                        "KTW"
                    ]
                },
                {
                    "id": "krakow",
                    "name": "Kraków",
                    "departureCodes": [
                        "KKQ",
                        "KRK"
                    ]
                },
                {
                    "id": "lublin",
                    "name": "Lublin",
                    "departureCodes": [
                        "LUL",
                        "LUZ"
                    ]
                },
                {
                    "id": "lodz",
                    "name": "Łód",
                    "departureCodes": [
                        "LCJ",
                        "LDZ"
                    ]
                },
                {
                    "id": "poznan",
                    "name": "Poznań",
                    "departureCodes": [
                        "POZ",
                        "PZP"
                    ]
                },
                {
                    "id": "rzeszow",
                    "name": "Rzeszów",
                    "departureCodes": [
                        "RZE",
                        "RZW"
                    ]
                },
                {
                    "id": "szczecin",
                    "name": "Szczecin",
                    "departureCodes": [
                        "SCE",
                        "SZZ"
                    ]
                },
                {
                    "id": "warszawa",
                    "name": "Warszawa",
                    "departureCodes": [
                        "WAW",
                        "WZZ"
                    ]
                },
                {
                    "id": "wroclaw",
                    "name": "Wrocław",
                    "departureCodes": [
                        "WRO",
                        "WRP"
                    ]
                }
            ],
            "multiple": true,
            "selfValidated": true
        },
        "destinationRegion": {
            "id": "destinationRegion",
            "name": "Dokšd",
            "type": "enum",
            "multiple": true,
            "values": [
                {
                    "id": "agadir-province",
                    "name": "Agadir",
                    "parentId": "maroko"
                },
                {
                    "id": "alanya-province",
                    "name": "Alanya",
                    "parentId": "turcja"
                },
                {
                    "id": "albania",
                    "name": "Albania"
                },
                {
                    "id": "algarve-province",
                    "name": "Algarve",
                    "parentId": "portugalia"
                },
                {
                    "id": "antalya-province",
                    "name": "Antalya",
                    "parentId": "turcja"
                },
                {
                    "id": "belek-province",
                    "name": "Belek",
                    "parentId": "turcja"
                },
                {
                    "id": "bodrum-province",
                    "name": "Bodrum",
                    "parentId": "turcja"
                },
                {
                    "id": "bulgaria",
                    "name": "Bułgaria"
                },
                {
                    "id": "cayo-santa-maria-province",
                    "name": "Cayo Santa Maria",
                    "parentId": "kuba"
                },
                {
                    "id": "cesme-province",
                    "name": "Cesme",
                    "parentId": "turcja"
                },
                {
                    "id": "chalkidiki-province",
                    "name": "Chalkidiki",
                    "parentId": "grecja"
                },
                {
                    "id": "chorwacja",
                    "name": "Chorwacja"
                },
                {
                    "id": "costa-blanca-province",
                    "name": "Costa Blanca",
                    "parentId": "hiszpania"
                },
                {
                    "id": "costa-brava-province",
                    "name": "Costa Brava",
                    "parentId": "hiszpania"
                },
                {
                    "id": "costa-calida-mar-menor-province",
                    "name": "Costa Calida  Mar Menor",
                    "parentId": "hiszpania"
                },
                {
                    "id": "costa-de-la-luz-province",
                    "name": "Costa de la Luz",
                    "parentId": "hiszpania"
                },
                {
                    "id": "costa-del-sol-province",
                    "name": "Costa del Sol",
                    "parentId": "hiszpania"
                },
                {
                    "id": "costa-dorada-province",
                    "name": "Costa Dorada",
                    "parentId": "hiszpania"
                },
                {
                    "id": "cypr",
                    "name": "Cypr"
                },
                {
                    "id": "didyma-province",
                    "name": "Didyma",
                    "parentId": "turcja"
                },
                {
                    "id": "dominikana",
                    "name": "Dominikana"
                },
                {
                    "id": "durres-province",
                    "name": "Durres",
                    "parentId": "albania"
                },
                {
                    "id": "egipt",
                    "name": "Egipt"
                },
                {
                    "id": "finike-province",
                    "name": "Finike",
                    "parentId": "turcja"
                },
                {
                    "id": "formentera-province",
                    "name": "Formentera",
                    "parentId": "hiszpania"
                },
                {
                    "id": "fuerteventura-province",
                    "name": "Fuerteventura",
                    "parentId": "wyspy-kanaryjskie"
                },
                {
                    "id": "gomera-province",
                    "name": "Gomera",
                    "parentId": "wyspy-kanaryjskie"
                },
                {
                    "id": "gran-canaria-province",
                    "name": "Gran Canaria",
                    "parentId": "wyspy-kanaryjskie"
                },
                {
                    "id": "grecja",
                    "name": "Grecja"
                },
                {
                    "id": "gruzja",
                    "name": "Gruzja"
                },
                {
                    "id": "hammamet-province",
                    "name": "Hammamet",
                    "parentId": "tunezja"
                },
                {
                    "id": "hiszpania",
                    "name": "Hiszpania"
                },
                {
                    "id": "holguin-province",
                    "name": "Holguin",
                    "parentId": "kuba"
                },
                {
                    "id": "hurghada-province",
                    "name": "Hurghada",
                    "parentId": "egipt"
                },
                {
                    "id": "ibiza-province",
                    "name": "Ibiza",
                    "parentId": "hiszpania"
                },
                {
                    "id": "kalabria-province",
                    "name": "Kalabria",
                    "parentId": "wlochy"
                },
                {
                    "id": "kemer-province",
                    "name": "Kemer",
                    "parentId": "turcja"
                },
                {
                    "id": "kenia",
                    "name": "Kenia"
                },
                {
                    "id": "korfu-province",
                    "name": "Korfu",
                    "parentId": "grecja"
                },
                {
                    "id": "kos-province",
                    "name": "Kos",
                    "parentId": "grecja"
                },
                {
                    "id": "krabi-province",
                    "name": "Krabi",
                    "parentId": "tajlandia"
                },
                {
                    "id": "kreta-province",
                    "name": "Kreta",
                    "parentId": "grecja"
                },
                {
                    "id": "kuba",
                    "name": "Kuba"
                },
                {
                    "id": "kusadasi-province",
                    "name": "Kusadasi",
                    "parentId": "turcja"
                },
                {
                    "id": "lanzarote-province",
                    "name": "Lanzarote",
                    "parentId": "wyspy-kanaryjskie"
                },
                {
                    "id": "larnaka-province",
                    "name": "Larnaka",
                    "parentId": "cypr"
                },
                {
                    "id": "lefkada-province",
                    "name": "Lefkada",
                    "parentId": "grecja"
                },
                {
                    "id": "madera",
                    "name": "Madera"
                },
                {
                    "id": "majorka-province",
                    "name": "Majorka",
                    "parentId": "hiszpania"
                },
                {
                    "id": "marmaris-province",
                    "name": "Marmaris",
                    "parentId": "turcja"
                },
                {
                    "id": "maroko",
                    "name": "Maroko"
                },
                {
                    "id": "marsa-el-alam-province",
                    "name": "Marsa El Alam",
                    "parentId": "egipt"
                },
                {
                    "id": "minorka-province",
                    "name": "Minorka",
                    "parentId": "hiszpania"
                },
                {
                    "id": "monastir-province",
                    "name": "Monastir",
                    "parentId": "tunezja"
                },
                {
                    "id": "oman",
                    "name": "Oman"
                },
                {
                    "id": "pafos-province",
                    "name": "Pafos",
                    "parentId": "cypr"
                },
                {
                    "id": "peloponez-province",
                    "name": "Peloponez",
                    "parentId": "grecja"
                },
                {
                    "id": "phuket-province",
                    "name": "Phuket",
                    "parentId": "tajlandia"
                },
                {
                    "id": "porto-santo",
                    "name": "Porto Santo"
                },
                {
                    "id": "portugalia",
                    "name": "Portugalia"
                },
                {
                    "id": "preveza-province",
                    "name": "Preveza",
                    "parentId": "grecja"
                },
                {
                    "id": "puerto-plata-province",
                    "name": "Puerto Plata",
                    "parentId": "dominikana"
                },
                {
                    "id": "punta-cana-province",
                    "name": "Punta Cana",
                    "parentId": "dominikana"
                },
                {
                    "id": "rodos-province",
                    "name": "Rodos",
                    "parentId": "grecja"
                },
                {
                    "id": "saranda-province",
                    "name": "Saranda",
                    "parentId": "albania"
                },
                {
                    "id": "sardynia-province",
                    "name": "Sardynia",
                    "parentId": "wlochy"
                },
                {
                    "id": "sharm-el-sheikh-province",
                    "name": "Sharm El Sheikh",
                    "parentId": "egipt"
                },
                {
                    "id": "side-province",
                    "name": "Side",
                    "parentId": "turcja"
                },
                {
                    "id": "sloneczny-brzeg-province",
                    "name": "Słoneczny Brzeg",
                    "parentId": "bulgaria"
                },
                {
                    "id": "sousse-province",
                    "name": "Sousse",
                    "parentId": "tunezja"
                },
                {
                    "id": "sozopol-province",
                    "name": "Sozopol",
                    "parentId": "bulgaria"
                },
                {
                    "id": "sycylia-province",
                    "name": "Sycylia",
                    "parentId": "wlochy"
                },
                {
                    "id": "tajlandia",
                    "name": "Tajlandia"
                },
                {
                    "id": "teneryfa-province",
                    "name": "Teneryfa",
                    "parentId": "wyspy-kanaryjskie"
                },
                {
                    "id": "thassos-province",
                    "name": "Thassos",
                    "parentId": "grecja"
                },
                {
                    "id": "tunezja",
                    "name": "Tunezja"
                },
                {
                    "id": "turcja",
                    "name": "Turcja"
                },
                {
                    "id": "wlochy",
                    "name": "Włochy"
                },
                {
                    "id": "wyspy-kanaryjskie",
                    "name": "Wyspy Kanaryjskie"
                },
                {
                    "id": "wyspy-zielonego-przyladka",
                    "name": "Wyspy Zielonego Przylšdka"
                },
                {
                    "id": "zakynthos-province",
                    "name": "Zakynthos",
                    "parentId": "grecja"
                },
                {
                    "id": "zanzibar",
                    "name": "Zanzibar"
                },
                {
                    "id": "zjednoczone-emiraty-arabskie",
                    "name": "Zjednoczone Emiraty Arabskie"
                },
                {
                    "id": "zlote-piaski-province",
                    "name": "Złote Piaski",
                    "parentId": "bulgaria"
                }
            ]
        },
        "tripActivities": {
            "id": "tripActivities",
            "name": "Aktywnoć",
            "type": "enum",
            "multiple": true,
            "values": [
                {
                    "id": "expedition",
                    "name": "Ekspedycje"
                },
                {
                    "id": "photography",
                    "name": "Fotograficzne"
                },
                {
                    "id": "horse-riding",
                    "name": "Jazda konna"
                },
                {
                    "id": "kayak",
                    "name": "Kajak"
                },
                {
                    "id": "multi-active",
                    "name": "Multi-aktywne"
                },
                {
                    "id": "diving",
                    "name": "Nurkowanie"
                },
                {
                    "id": "fitness",
                    "name": "Obozy fitness"
                },
                {
                    "id": "off-road",
                    "name": "Off-road"
                },
                {
                    "id": "rafting",
                    "name": "Rafting"
                },
                {
                    "id": "bike",
                    "name": "Rower"
                },
                {
                    "id": "surfing",
                    "name": "Surfing/windsurfing"
                },
                {
                    "id": "trekking",
                    "name": "Trekking"
                },
                {
                    "id": "sails",
                    "name": "Żagle"
                }
            ]
        },
        "adultsNumber": {
            "id": "adultsNumber",
            "name": "Doroli",
            "type": "integer",
            "minValue": 1,
            "maxValue": 6
        },
        "childrenAge": {
            "id": "childrenAge",
            "type": "array",
            "name": "Dzieci"
        }
    }
}