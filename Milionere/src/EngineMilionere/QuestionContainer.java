package EngineMilionere;

public class QuestionContainer {
	// data object
	private int numberQuestion;
	private String question;
	private String answerA;
	private String answerB;
	private String answerC;
	private String answerD;

	private char correctAnswer;

	// constructor
	public QuestionContainer(int numberQuetion, String question, String answerA, String answerB, String answerC,
			String answerD, char correctAnswer) {
		super();
		this.numberQuestion = numberQuetion;
		this.question = question;
		this.answerA = answerA;
		this.answerB = answerB;
		this.answerC = answerC;
		this.answerD = answerD;

		this.correctAnswer = Character.toUpperCase(correctAnswer);
	}

	// get function
	public int getNumberQuestion() {
		return numberQuestion;
	}

	public String getQuestion() {
		return question;
	}

	public String getAnswerA() {
		return answerA;
	}

	public String getAnswerB() {
		return answerB;
	}

	public String getAnswerC() {
		return answerC;
	}

	public String getAnswerD() {
		return answerD;
	}

	// boolean function
	public boolean isCorrectAnswer(char answer) {
		return this.correctAnswer == Character.toUpperCase(answer);

	}
}
