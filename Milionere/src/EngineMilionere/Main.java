package EngineMilionere;



import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		//Test Container class
		QuestionContainer q1 = new QuestionContainer(1, "Kt�ra liczba nie jest sta��", "PI", "E", "G", "Z", 'D');

		System.out.println("Pyt " + q1.getNumberQuestion() + "\t:");
		System.out.printf("%s\nA:\t%s\tB:\t%s\nC:\t%s\tD:\t%s\n", q1.getQuestion(), q1.getAnswerA(), q1.getAnswerB(),
				q1.getAnswerC(), q1.getAnswerD());

		Scanner answer = new Scanner(System.in);

		System.out.println("Odpowiedz:\t");
		if (q1.isCorrectAnswer(answer.next().charAt(0))) {
			System.out.println("Gratulacje odpowiedz poprawna");
		} else {
			System.out.println("Przykro mi. Odpowiedz nie poprawna");
		}

		answer.close();
	}

}
